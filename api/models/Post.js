const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = new Schema({
  title: {
    type: String,
    require: true,
  },
  user:{
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  description: String,
  image: String,
  datetime: String,
});

const Post = mongoose.model('Post', PostSchema);
module.exports = Post;