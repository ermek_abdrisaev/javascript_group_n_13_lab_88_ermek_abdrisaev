const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CommentsSchema = new Schema({
  user:{
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  post:{
    type: Schema.Types.ObjectId,
    ref: 'Post'
  },
  text: String,
});

const Comments = mongoose.model('Comments', CommentsSchema);
module.exports = Comments;