const mongoose = require('mongoose');
const config = require('./config');
const Post = require('./models/Post');
const Comments = require('./models/Comments');
const User = require('./models/User');

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);
  const collections = await mongoose.connection.db.listCollections().toArray();
  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [Test1, AliBaba, Ded] = await User.create({
    email: 'test6@test.ru',
    displayName: 'Test1',
    token: 'wjtwus6Gv2aomXFTtcMXA',
    _id: '622bb0ff40f525a01766918d',
    password: 'test'
  },{
    displayName: 'AliBaba',
    email: 'alibaba@baba.com',
    token: '3HZ5OB2FgPJdqRi4COI6K',
    _id: '622bb175354835a2d4d0a8a7',
    password: 'alibaba'
  },{
    displayName: 'Ded',
    email: 'ded@hasan.gz',
    token: 'LZSDq_MPwpOJEmrIlFhIq',
    _id: '622bb1bdf246638e762589d6',
    password: 'ded'
  })

  await Post.create({
    title: 'Im test post',
    user: Test1,
    description: 'Test is good thing to check',
    image: '0HFfoTbajOiEvlEnj6CwJ.jpeg',
    datetime: '2022-03-11: 10:22'
  }, {
    title: '1001 nights',
    user: AliBaba,
    description: 'I love stories for night',
    image: 'k5GhKoV4I13JiUo3Kcs1A.jpeg',
    datetime: '2022-03-11: 12:12'
  }, {
    title: 'CrimePrime',
    user: Ded,
    description: 'Crimes of all countries in one place - crimeprime.ru',
    image: 'MsNhBrXJxJryEyyQbxo-x.jpeg',
    datetime: '2022-03-11: 10:12'
  });

  await Comments.create({
    user: '622bb0ff40f525a01766918d',
    post: '622babe5c9d91d3daf18387c',
    text: 'go test yourself)))'
  },{
    user:'622bb175354835a2d4d0a8a7',
    post: '6229cbf9b847cc13f2205e77',
    text: 'stories is good'
  },{
    user: '622bb1bdf246638e762589d6',
    post: '6229b83f60595a2b93373fb4',
    text: 'all crimes must be in jail',
  });

  await mongoose.connection.close();
};

run().catch(e => console.error(e));