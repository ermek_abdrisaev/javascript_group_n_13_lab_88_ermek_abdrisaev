const express = require('express');
const Comments = require('../models/Comments');
const auth = require("../middlewear/auth");

const router = express.Router();

router.get('/', async (req, res, next) => {
  try {
    const query = {};
    if (req.query.post) {
      const commentId = await Comment.find({post: {_id: req.query.post}}).populate('post', 'description');
      return res.send(commentId);
    }
    const comment = await Comments.find(query).populate('post', 'description');

    return res.send(comment);
  } catch (e) {
    next(e);
  }
})

router.post('/', auth, async (req, res, next) => {
  try {
    const commentData = {
      user: req.user._id,
      text: req.body.text,
      post: req.body.post
    };
    const comment = new Comments(commentData);
    await comment.save();
    return res.send(comment);
  } catch (e) {
    next(e);
  }
});
module.exports = router;