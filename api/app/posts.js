const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Post = require('../models/Post');
const mongoose = require("mongoose");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    const query = {};

    if (req.query.filter === 'image') {
      query.image = {$ne: null};
    }

    const post = await Post.find(query).populate('user', 'displayName');
    return res.send(post);
  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return false;
    const postOne = await Post.findOne({_id: req.params.id}).populate('user', 'displayName');
    return res.send(postOne);
  } catch (e) {
    next(e);
  }
});

router.post('/', upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.title) {
      return res.status(400).send({message: 'Title are required!!!'});
    }
    const postData = {
      comments: req.body.comments,
      title: req.body.title,
      description: req.body.description,
      image: null,
      datetime: new Date().toISOString(),
    };

    if (req.file) {
      postData.image = req.file.filename;
    }

    const post = new Post(postData);
    await post.save();

    return res.send({message: 'Created new post', id: post._id})
  } catch (e) {
    next(e);
  }
});
module.exports = router;