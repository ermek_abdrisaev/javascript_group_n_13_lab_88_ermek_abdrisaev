import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { AppState } from '../store/types';
import { Store } from '@ngrx/store';
import { ApiPostsData, Post } from '../models/post.model';
import { fetchPostRequest } from '../store/posts.actions';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { CommentData } from '../models/comment.model';
import { createCommentRequest } from '../store/comments.actions';
import { User } from '../models/user.model';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  commentLoading: Observable<boolean>;
  commentError: Observable<string | null>;
  post: Observable<Post | null>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  postId!: ApiPostsData;
  user: Observable<null | User>;

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.post = store.select(state => state.posts.post);
    this.loading = store.select(state => state.posts.fetchLoading);
    this.error = store.select(state => state.posts.fetchError);
    this.commentLoading = store.select(state => state.comments.createLoading);
    this.commentError = store.select(state => state.comments.createError);
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.store.dispatch(fetchPostRequest({id: params['id']}));
    });

    this.post.subscribe(post => {
      this.postId = <ApiPostsData>post;
    });
  }

  onSubmit() {
    const commentData: CommentData = this.form.value;
    commentData.post = this.postId._id;
    this.user.subscribe( user => {
      if (user) {
        commentData.token = user?.token
      }
    })
    this.store.dispatch(createCommentRequest({commentData}));
  }
}
