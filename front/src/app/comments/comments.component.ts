import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Comment } from '../models/comment.model';
import { AppState } from '../store/types';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { fetchCommentRequest } from '../store/comments.actions';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.sass']
})
export class CommentsComponent implements OnInit {
  comments: Observable<Comment[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.comments = store.select(state => state.comments.comments);
    this.loading = store.select(state => state.comments.fetchLoading);
    this.error = store.select(state => state.comments.fetchError);
  }

  ngOnInit(): void {
    this.route.params.subscribe(post => {
      this.store.dispatch(fetchCommentRequest({id: post['id']}));
    });
  }
}
