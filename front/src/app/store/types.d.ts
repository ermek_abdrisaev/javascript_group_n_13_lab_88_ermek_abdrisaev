import { Post } from '../models/post.model';
import { LoginError, RegisterError, User } from '../models/user.model';
import { Comment } from '../models/comment.model';

export type PostsState = {
  posts: Post[],
  post: Post | null,
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
};

export type UsersState = {
  user: null | User,
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError,
}

export type CommentsState = {
  comments: Comment[],
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
}

export type AppState = {
  posts: PostsState,
  users: UsersState
  comments: CommentsState,
}
