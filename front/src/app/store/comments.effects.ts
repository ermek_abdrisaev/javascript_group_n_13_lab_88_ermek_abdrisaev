import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import { CommentsService } from '../services/comments.service';
import { Router } from '@angular/router';
import {
  createCommentFailure,
  createCommentRequest,
  createCommentSuccess,
  fetchCommentFailure,
  fetchCommentRequest,
  fetchCommentSuccess
} from './comments.actions';

@Injectable()
export class CommentsEffects {

  fetchComment = createEffect(() => this.actions.pipe(
    ofType(fetchCommentRequest),
    mergeMap(id => this.commentsService.getComment(id.id).pipe(
      map(comments => fetchCommentSuccess({comments})),
      catchError(() => of(fetchCommentFailure({error: 'Something went wrong!!!'})))
    ))
  ));

  createComment = createEffect(() => this.actions.pipe(
    ofType(createCommentRequest),
    mergeMap(({commentData}) => this.commentsService.createComment(commentData).pipe(
        map(() => createCommentSuccess()),
        tap(() => {
          return this.router.navigate(['/']);
        }),
        catchError(() => {
          return of(createCommentFailure({
            error: 'Wrong data!'
          }));
        })
      )
    )
  ));

  constructor(
    private actions: Actions,
    private commentsService: CommentsService,
    private router: Router
  ) {
  }
}
