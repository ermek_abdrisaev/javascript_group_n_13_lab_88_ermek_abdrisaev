import { CommentsState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createCommentFailure,
  createCommentRequest,
  createCommentSuccess,
  fetchCommentFailure,
  fetchCommentRequest,
  fetchCommentSuccess
} from './comments.actions';

const initialState: CommentsState = {
  comments: [],
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null
};

export const commentsReducer = createReducer(
  initialState,

  on(fetchCommentRequest, state => ({...state, fetchLoading: true})),
  on(fetchCommentSuccess, (state, {comments}) => ({...state, fetchLoading: false, comments})),
  on(fetchCommentFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(createCommentRequest, state => ({...state, createLoading: true})),
  on(createCommentSuccess, state => ({...state, createLoading: false})),
  on(createCommentFailure, (state, {error}) => ({...state, createLoading: false, createError: error}))
);
