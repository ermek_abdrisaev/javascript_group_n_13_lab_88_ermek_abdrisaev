import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  createPostFailure,
  createPostRequest,
  createPostSuccess,
  fetchPostFailure,
  fetchPostRequest,
  fetchPostsFailure,
  fetchPostsRequest,
  fetchPostsSuccess,
  fetchPostSuccess
} from './posts.actions';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import { PostsService } from '../services/posts.services';
import { Router } from '@angular/router';

@Injectable()
export class PostsEffects {

  fetchPosts = createEffect(() => this.actions.pipe(
    ofType(fetchPostsRequest),
    mergeMap(() => this.postsService.getPosts().pipe(
      map(posts => fetchPostsSuccess({posts})),
      catchError(() => of(fetchPostsFailure({error: 'Something went wrong'})))
    ))
  ));

  fetchPost = createEffect(() => this.actions.pipe(
    ofType(fetchPostRequest),
    mergeMap(id => this.postsService.getPost(id.id).pipe(
      map(post => fetchPostSuccess({post})),
      catchError(() => of(fetchPostFailure({error: 'Something went wrong'})))
    ))
  ));

  createPost = createEffect(() => this.actions.pipe(
    ofType(createPostRequest),
    mergeMap(({postsData}) => this.postsService.createPost(postsData).pipe(
        map(() => createPostSuccess()),
        tap(() => {
          return this.router.navigate(['/']);
        }),
        catchError(() => {
          return of(createPostFailure({
            error: 'Wrong data'
          }));
        })
      )
    )
  ));

  constructor(
    private actions: Actions,
    private postsService: PostsService,
    private router: Router
  ) {
  }
}
