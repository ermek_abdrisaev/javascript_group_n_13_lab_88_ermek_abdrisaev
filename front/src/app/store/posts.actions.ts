import { createAction, props } from '@ngrx/store';
import { Post, PostsData } from '../models/post.model';

export const fetchPostsRequest = createAction(
  '[Post] Fetch Request');
export const fetchPostsSuccess = createAction(
  '[Post] Fetch Success',
  props<{ posts: Post[] }>()
);
export const fetchPostsFailure = createAction(
  '[Post] Fetch Failure',
  props<{ error: string }>()
);

export const fetchPostRequest = createAction(
  '[Post] Fetch Request',
  props<{ id: string }>()
);
export const fetchPostSuccess = createAction(
  '[Post] Fetch Success',
  props<{ post: Post }>()
);
export const fetchPostFailure = createAction(
  '[Post] Fetch Failure',
  props<{ error: string }>()
);

export const createPostRequest = createAction(
  '[Posts] Create Request',
  props<{ postsData: PostsData }>()
);
export const createPostSuccess = createAction(
  '[Posts] Create Success'
);
export const createPostFailure = createAction(
  '[Posts] Create Failure',
  props<{ error: string }>()
);
