import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { NgForm } from '@angular/forms';
import { AppState } from '../store/types';
import { Store } from '@ngrx/store';
import { PostsData } from '../models/post.model';
import { createPostRequest } from '../store/posts.actions';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.sass']
})
export class NewPostComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  @Input()postId!: string;
  loading: Observable<boolean>;
  error: Observable<string | null>;

  constructor(private store: Store<AppState>) {
    this.loading = store.select(state => state.posts.createLoading);
    this.error = store.select(state => state.posts.createError);
  }

  ngOnInit(): void {
  }

  onSubmit() {
    const postsData: PostsData = this.form.value;
    this.store.dispatch((createPostRequest({postsData})));
  }
}
