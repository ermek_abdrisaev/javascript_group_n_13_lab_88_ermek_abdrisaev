export class Post {
  constructor(
    public _id: string,
    public title: string,
    public description: string,
    public image: string,
    public datetime: string,
    public user: {
      _id: string,
      displayName: string,
    },
  ) {
  }
}

export interface PostsData {
  title: string;
  description: string;
  image: File | null;
  datetime: string;
  user: {
    _id: string,
    displayName: string,
  };
}

export interface ApiPostsData {
  _id: string,
  title: string,
  description: string,
  image: string,
  datetime: string,
  user: {
    _id: string,
    displayName: string,
  }
}
