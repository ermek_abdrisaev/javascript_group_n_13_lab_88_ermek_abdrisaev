export class Comment {
  constructor(
    public _id: string,
    public user: string,
    public post: string,
    public text: string,
  ) {
  }
}

export interface CommentData {
  [key: string]: any;
  post: string;
  token: string;
  text: string;
}

export interface ApiCommentData {
  _id: string,
  post: string,
  user: string,
  text: string
}
