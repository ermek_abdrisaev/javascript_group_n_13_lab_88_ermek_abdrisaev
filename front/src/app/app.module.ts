import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutComponent } from './ui/layout/layout.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PostsComponent } from './posts/posts.component';
import { FlexLayoutModule, FlexModule } from '@angular/flex-layout';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatCardModule } from '@angular/material/card';
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { postsReducer } from './store/posts.reducer';
import { usersReducer } from './store/users.reducer';
import { PostsEffects } from './store/posts.effects';
import { UsersEffects } from './store/users.effects';
import { ImagePipe } from './pipes/image.pipe';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { FileInputComponent } from './ui/file-input/file-input.component';
import { LoginComponent } from './login/login.component';
import { CenteredCardComponent } from './centered-card/centered-card.component';
import { MatInputModule } from '@angular/material/input';
import { RegisterComponent } from './register/register.component';
import { NewPostComponent } from './new-post/new-post.component';
import { localStorageSync } from 'ngrx-store-localstorage';
import { MatMenuModule } from '@angular/material/menu';
import { CommentsComponent } from './comments/comments.component';
import { commentsReducer } from './store/commentsreducer';
import { PostComponent } from './post/post.component';
import { CommentsEffects } from './store/comments.effects';
import { MatBadgeModule } from '@angular/material/badge';

const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{users: ['user']}],
    rehydrate: true,
  })(reducer);
};

const metaReducers: MetaReducer[] = [localStorageSyncReducer];

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    PostsComponent,
    ImagePipe,
    FileInputComponent,
    LoginComponent,
    CenteredCardComponent,
    RegisterComponent,
    NewPostComponent,
    CommentsComponent,
    PostComponent,
    CenteredCardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    FlexLayoutModule,
    HttpClientModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatFormFieldModule,
    FlexModule,
    MatProgressSpinnerModule,
    MatCardModule,
    StoreModule.forRoot({
      posts: postsReducer,
      users: usersReducer,
      comments: commentsReducer,
    }, {metaReducers}),
    EffectsModule.forRoot([
      PostsEffects,
      UsersEffects,
      CommentsEffects,
    ]),
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatInputModule,
    MatMenuModule,
    MatBadgeModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
