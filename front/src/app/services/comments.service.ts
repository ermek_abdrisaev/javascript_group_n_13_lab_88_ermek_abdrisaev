import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiCommentData, CommentData } from '../models/comment.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {
  constructor(private http: HttpClient) {
  }

  getComment(id: string) {
    return this.http.get<ApiCommentData[]>(environment.apiUrl + `/comments?posts=${id}`).pipe(
      map(result => {
        return result;
      })
    );
  }

  createComment(commentData: CommentData) {
    console.log(commentData);
    return this.http.post(environment.apiUrl + '/comments', commentData, {
      headers: new HttpHeaders({'Authorization': commentData.token})})
  }
}
