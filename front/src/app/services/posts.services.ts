import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiPostsData, Post, PostsData } from '../models/post.model';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class PostsService {
  constructor(private http: HttpClient) {
  }

  getPosts() {
    return this.http.get<ApiPostsData[]>(environment.apiUrl + '/posts').pipe(
      map(response => {
        return response.map(postData => {
          return new Post(
            postData._id,
            postData.title,
            postData.description,
            postData.image,
            postData.datetime,
            postData.user,
          );
        });
      })
    );
  }

  getPost(id: string) {
    return this.http.get<ApiPostsData>(environment.apiUrl + `/posts/${id}`).pipe(
      map(result => {
        return result;
      })
    );
  }

  createPost(postData: PostsData) {
    const formData = new FormData();
    formData.append('title', postData.title);
    formData.append('description', postData.description);
    if (postData.image) {
      formData.append('image', postData.image);
    }
    return this.http.post(environment.apiUrl + '/posts', formData);
  }
}
